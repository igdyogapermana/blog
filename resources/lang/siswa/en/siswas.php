<?php

return [
    'list resource' => 'List siswas',
    'create resource' => 'Create siswas',
    'edit resource' => 'Edit siswas',
    'destroy resource' => 'Destroy siswas',
    'title' => [
        'siswas' => 'Siswa',
        'create siswa' => 'Create a siswa',
        'edit siswa' => 'Edit a siswa',
    ],
    'fields' => [
        'nama' => 'Name',
        'umur' => 'Age',
        'gambar' => 'Image'
    ],
    'jurusan' => [
        'title' => [
            'jurusans' => 'Jurusan',
            'create jurusan' => 'Create a jurusan',
            'edit jurusan' => 'Edit a jurusan'
        ],
        'fields' => [
            'nama jurusan' => 'Name Jurusan'
        ],
        'button' => [
            'create jurusan' => 'Create a jurusan'
        ]
    ],
    'kategori' => [
        'title' => [
            'kategories' => 'Categories',
            'create kategori' => 'Create a categories',
            'edit kategori' => 'Edit a categories'
        ],
        'fields' => [
            'nama kategori' => 'Name Categories'
        ],
        'buttons' => [
            'create kategori' => 'Create a categories'
        ],
        'messages' => [
            'create kategori' => 'Data created !',
            'update kategori' => 'Data updated !'
        ]
    ],
    'barang' => [
        'title' => [
            'barangs' => 'Product',
            'create barang' => 'Create a product',
            'edit barang' => 'Edit a barang' 
        ],
        'fields' => [
            'nama barang' => 'Name Product',
            'kategori' => 'Categories',
            'gambar' => 'Image Product'
        ],
        'buttons' => [
            'create' => 'Create a product',
        ],
        'messages' => [
            'create barang' => 'Data created !',
            'update barang' => 'Data updated !'
        ]
    ],
    'button' => [
        'create siswa' => 'Create a siswa',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
