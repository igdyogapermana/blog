<?php

namespace Modules\Siswa\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

Use Modules\Siswa\Entities\Kategori;
Use Modules\Siswa\Entities\Barang;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('siswa::index');
    }
    public function store(Request $request){
        $data = [
            'nama_barang' => $request->nama_barang,
            'kategori_id' => $request->kategori,
            'gambar' => $request->gambar['imgName'][0]            
        ];
        dd($data);
    }


}
