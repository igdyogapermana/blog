<?php

namespace Modules\Siswa\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateKategoriRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_kategori' => 'required|string|min:3',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages(){
        return [
            'nama_kategori.min' => ':attribute must be at least 3 characters !'
        ];
    }

    public function attributes(){
        return [
            'nama_kategori' => trans('siswa::siswas.kategori.fields.nama kategori')
        ];
    }

    
}
