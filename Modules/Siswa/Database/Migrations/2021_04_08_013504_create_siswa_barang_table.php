<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiswaBarangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswa__barangs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_barang');
            $table->unsignedInteger('kategori_id');
            $table->foreign('kategori_id')->references('id')->on('siswa__kategories')->onDelete('cascade');
            $table->string('gambar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswa__barang');
    }
}
