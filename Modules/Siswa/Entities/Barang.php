<?php

namespace Modules\Siswa\Entities;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = 'siswa__barangs';
    protected $fillable = ['nama_barang' , 'kategori_id' , 'gambar'];

    public function kategori(){
        return $this->belongsTo(Kategori::class , 'kategori_id' , 'id');
    }

}
